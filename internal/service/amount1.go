package service

import (
	"context"
	amount_v1 "gorm_transaction/api/Amount1Server/v1"
	"gorm_transaction/internal/biz"
)

type Amount1Service struct {
	amount_v1.UnimplementedAmount1ServiceServer

	bz *biz.Amount1BizStruct
}

func NewAmount1Service(bz *biz.Amount1BizStruct) *Amount1Service {
	return &Amount1Service{
		bz: bz,
	}
}

func (a *Amount1Service) TransOutOrder(ctx context.Context, req *amount_v1.TransOutOrderRequest) (*amount_v1.Empty, error) {
	return a.bz.TransOutOrder(ctx, req)
}

func (a *Amount1Service) TransOutOrderCompensate(ctx context.Context, req *amount_v1.TransOutOrderCompensateRequest) (*amount_v1.Empty, error) {
	return a.bz.TransOutOrderCompensate(ctx, req)
}
