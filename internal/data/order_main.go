package data

import (
	"context"
	"gorm.io/gorm"
	"gorm_transaction/internal/biz"
)

// 不在事务中执行，初始化自己的db
func (a *amount1Repo) EditOrderMain(ctx context.Context, id uint32, changeValue int64) error {
	db := a.data.Mysql.Table(biz.OrderMainTableName).WithContext(ctx)

	// stock = stock + changeValue Notice 并发安全的修改方式
	err := db.Where("id = ?", id).UpdateColumn("stock", gorm.Expr("stock + ?", changeValue)).Error

	return err
}

// 在事务中执行，这里的db其实是ctx中携带的tx（在InTx方法中加进去的）
func (a *amount1Repo) EditOrderMainWithTX(ctx context.Context, id uint32, changeValue int64) error {

	// 注意需要指定表名
	db := a.data.DB(ctx).Table(biz.OrderMainTableName)

	// stock = stock + changeValue Notice 并发安全的修改方式
	err := db.Where("id = ?", id).UpdateColumn("stock", gorm.Expr("stock + ?", changeValue)).Error

	return err
}
