package data

import (
	"context"
	"gorm_transaction/internal/biz"
)

// 不在事务中执行，初始化自己的db
func (a *amount1Repo) AddOrderStatement(ctx context.Context, orderMainId uint32, changeValue int64, orderNo string) error {
	db := a.data.Mysql.Table(biz.OrderStatementTableName).WithContext(ctx)

	// 直接新增一条流水记录
	statement := biz.OrderStatement{
		OrderMainId: orderMainId,
		OrderNo:     orderNo,
		ChangeValue: changeValue,
	}

	err := db.Create(&statement).Error

	return err
}

// 在事务中执行
func (a *amount1Repo) AddOrderStatementWithTX(ctx context.Context, orderMainId uint32, changeValue int64, orderNo string) error {

	// 注意需要指定表名
	db := a.data.DB(ctx).Table(biz.OrderStatementTableName)

	// 直接新增一条流水记录
	statement := biz.OrderStatement{
		OrderMainId: orderMainId,
		OrderNo:     orderNo,
		ChangeValue: changeValue,
	}

	err := db.Create(&statement).Error

	return err
}

// 不在事务中执行，初始化自己的db
func (a *amount1Repo) DelOrderStatement(ctx context.Context, id uint32, orderNo string) error {
	db := a.data.Mysql.Table(biz.OrderStatementTableName).WithContext(ctx)

	err := db.Where("order_main_id = ? and order_no = ?", id, orderNo).Update("is_delete", biz.OrderStatementDeleted).Error

	return err
}

// 在事务中执行
func (a *amount1Repo) DelOrderStatementWithTX(ctx context.Context, id uint32, orderNo string) error {

	// 注意需要指定表名
	db := a.data.DB(ctx).Table(biz.OrderStatementTableName)

	err := db.Where("order_main_id = ? and order_no = ?", id, orderNo).Update("is_delete", biz.OrderStatementDeleted).Error

	return err
}
