package registry

import (
	"github.com/go-kratos/kratos/contrib/registry/etcd/v2"
	v3 "go.etcd.io/etcd/client/v3"
	"gorm_transaction/internal/conf"
)

func NewEtcdRegistry(c *conf.Registry) *etcd.Registry {
	client, err := v3.New(v3.Config{
		Endpoints: []string{c.Etcd.Addr},
		Username:  c.Etcd.Username,
		Password:  c.Etcd.Password,
	})
	if err != nil {
		panic(err)
	}
	return etcd.New(client)
}
