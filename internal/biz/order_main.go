package biz

import "time"

const (
	OrderMainTableName = "order_main"
)

type OrderMain struct {
	Id        uint32     `gorm:"column:id;primary_key" json:"id"`
	Stock     uint64     `gorm:"column:stock" json:"stock"`
	CreatedAt *time.Time `gorm:"column:created_at;default:null" json:"createdAt"`
	UpdatedAt *time.Time `gorm:"column:updated_at;default:null" json:"updatedAt"`
}

func (o *OrderMain) TableName() string {
	return OrderMainTableName
}
