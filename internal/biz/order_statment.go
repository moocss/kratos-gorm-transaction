package biz

import "time"

const (
	OrderStatementTableName = "order_statement"
	OrderStatementDeleted   = 1
)

type OrderStatement struct {
	Id          uint32     `gorm:"column:id;primary_key" json:"id"`
	OrderMainId uint32     `gorm:"column:order_main_id" json:"orderMainId"`
	OrderNo     string     `gorm:"column:order_no" json:"orderNo"`
	ChangeValue int64      `gorm:"column:change_value" json:"change_value"`
	IsDelete    uint       `gorm:"column:is_delete" json:"isDelete"`
	CreatedAt   *time.Time `gorm:"column:created_at;default:null" json:"createdAt"`
	UpdatedAt   *time.Time `gorm:"column:updated_at;default:null" json:"updatedAt"`
}

func (o *OrderStatement) TableName() string {
	return OrderStatementTableName
}
