// Code generated by Wire. DO NOT EDIT.

//go:generate go run github.com/google/wire/cmd/wire
//go:build !wireinject
// +build !wireinject

package main

import (
	"gorm_transaction/internal/biz"
	"gorm_transaction/internal/conf"
	"gorm_transaction/internal/data"
	"gorm_transaction/internal/registry"
	"gorm_transaction/internal/server"
	"gorm_transaction/internal/service"
	"github.com/go-kratos/kratos/v2"
	"github.com/go-kratos/kratos/v2/log"
)

import (
	_ "go.uber.org/automaxprocs"
)

// Injectors from wire.go:

// wireApp init kratos application.
func wireApp(confServer *conf.Server, confData *conf.Data, confRegistry *conf.Registry, logger log.Logger) (*kratos.App, func(), error) {
	db := data.NewMysql(confData, logger)
	dataData, cleanup, err := data.NewData(confData, db, logger)
	if err != nil {
		return nil, nil, err
	}
	amount1BizInterface := data.NewAmount1Repo(dataData, logger)
	transaction := data.NewTransaction(dataData)
	amount1BizStruct := biz.NewAmount1Biz(amount1BizInterface, logger, transaction)
	amount1Service := service.NewAmount1Service(amount1BizStruct)
	grpcServer := server.NewGRPCServer(confServer, amount1Service, logger)
	httpServer := server.NewHTTPServer(confServer, logger)
	etcdRegistry := registry.NewEtcdRegistry(confRegistry)
	app := newApp(confServer, logger, grpcServer, httpServer, etcdRegistry)
	return app, func() {
		cleanup()
	}, nil
}
